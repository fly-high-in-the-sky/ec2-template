#!/bin/bash -xe
sudo yum update -y
sudo yum install git -y
sudo yum install python36 python36-virtualenv python36-pip -y
sudo ln -s /usr/bin/pip-3.6 /usr/bin/pip3
sudo pip3 install --upgrade pip
sudo ln -s /usr/local/bin/pip3 /usr/bin/pip3
cd /home/ec2-user
sudo pip3 install flask
sudo pip3 install requests
sudo pip3 install boto3
sudo git clone https://gitlab.com/stanma910716/sandbag-deploy.git
cd sandbag-deploy
sudo python3 redisHandler.py "$1"
sudo pip3 install -r sandbag/requirements.txt
sudo python3 server/server_v2_ECS.py