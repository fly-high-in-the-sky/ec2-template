#!/bin/bash -xe
sudo yum update -y
sudo yum install git -y
sudo yum install python3 python3-virtualenv python3-pip -y
sudo pip3 install --upgrade pip
sudo ln -s /usr/local/bin/pip3 /usr/bin/pip3
cd /home/ec2-user
sudo pip3 install flask
sudo pip3 install requests
sudo pip3 install boto3
sudo git clone https://gitlab.com/stanma910716/sandbag-deploy.git
cd sandbag-deploy
sudo pip3 install -r sandbag/requirements.txt
sudo python3 redisHandler.py "$1"
python3 client/client_multi.py