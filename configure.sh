!/bin/bash
yum update -y
yum install git -y
yum install python36 python36-virtualenv python36-pip -y
ln -s /usr/bin/pip-3.6 /usr/bin/pip3
pip3 install --upgrade pip
ln -s /usr/local/bin/pip3 /usr/bin/pip3
pip3 install flask
pip3 install requests
pip3 install boto3
pip3 install mysql.connector
cd /home/ec2-user
sudo git clone https://gitlab.com/stanma910716/jam-rds-read-repica
cd jam-rds-read-repica
python3 quizHandler.py
rm -rf quizHandler.py
python3 app.py