#!/usr/bin/env python3

import os.path
import base64

from aws_cdk.aws_s3_assets import Asset
from aws_cdk import (
    aws_ec2 as ec2,
    aws_iam as iam,
    aws_dynamodb as dynamodb,
    aws_elasticache as elasticache,
    aws_autoscaling as asg,
    core
)

dirname = os.path.dirname(__file__)
request_server_num = 1

class EC2Stack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        amzn_linux = ec2.MachineImage.latest_amazon_linux(
            generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX,
            edition=ec2.AmazonLinuxEdition.STANDARD,
            virtualization=ec2.AmazonLinuxVirt.HVM,
            storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
        )

        vpc = ec2.Vpc(
            self,
            "vpc-sandbag",
            subnet_configuration = [ec2.SubnetConfiguration(name="",subnet_type=ec2.SubnetType.PUBLIC)]
        )

        vpc_subnets = vpc.select_subnets(
            subnet_type=ec2.SubnetType.PUBLIC
        ).subnet_ids

        # vpc_subnets = vpc.select_subnets(
        #     subnet_type=ec2.SubnetType.PUBLIC
        # ).subnets

        web_sg = ec2.SecurityGroup(
            self,
            "SG-SandBag",
            vpc=vpc,
            security_group_name="SandBag_SG"
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(80)
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(22)
        )
        web_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp_range(
                start_port=1024,
                end_port=65535
            )
        )
        role = iam.Role(
            self,
            "Role-SandBag",
            assumed_by=iam.ServicePrincipal("ec2.amazonaws.com")
        )
        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonElastiCacheFullAccess"))
        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonSSMManagedInstanceCore"))
        
        ec_sg = elasticache.CfnSubnetGroup(
            self,
            "Elasticache-SubnetGroup",
            description="The subnet group for Elasticache",
            subnet_ids=vpc_subnets
        )
        ec_sg.node.add_dependency(vpc)

        ec_security = elasticache.CfnSecurityGroup(
            self,
            "Redis - SG",
            description="Redis - SG"
        )

        ec_security.node.add_dependency(ec_sg)

        ec = elasticache.CfnReplicationGroup(
            self,
            "SandBag - Redis",
            cache_node_type="cache.t3.micro",
            engine="redis",
            num_node_groups=1,
            replicas_per_node_group=2,
            replication_group_description="Read Replica",
            security_group_ids=[web_sg.security_group_id],
            cache_subnet_group_name=ec_sg.ref
        )
        ec.node.add_dependency(ec_security)

        rs_ag = asg.AutoScalingGroup(
            self,
            "Request Server",
            instance_type=ec2.InstanceType("t2.micro"),
            machine_image=amzn_linux,
            vpc=vpc,
            role=role,
            security_group=web_sg,
        )
        rs_ag.node.add_dependency(ec)

        application = ec2.Instance(
            self,
            "SandBag - Application",
            instance_type=ec2.InstanceType("c5.large"),
            vpc=vpc,
            role=role,
            machine_image=amzn_linux,
            security_group=web_sg
        )

        server = ec2.Instance(
            self,
            "SandBag - Client User",
            instance_type=ec2.InstanceType("t2.micro"),
            vpc=vpc,
            role=role,
            machine_image=amzn_linux,
            security_group=web_sg,
        )

        server.node.add_dependency(rs_ag)
        application.node.add_dependency(rs_ag)

        # Script in S3 as Asset
        client_asset = Asset(
            self,
            "SandBag - Client Userdata",
            path=os.path.join(dirname,"userdata/client.sh")
        )
        application_asset = Asset(
            self,
            "SandBag - Application Userdata",
            path=os.path.join(dirname,"userdata/application.sh")
        )
        server_asset = Asset(
            self,
            "SandBag - Server Userdata",
            path=os.path.join(dirname,"userdata/server.sh")
        )
        client_path = server.user_data.add_s3_download_command(
            bucket=client_asset.bucket,
            bucket_key=client_asset.s3_object_key
        )
        application_path = application.user_data.add_s3_download_command(
            bucket=application_asset.bucket,
            bucket_key=application_asset.s3_object_key
        )
        server_path = rs_ag.user_data.add_s3_download_command(
            bucket=server_asset.bucket,
            bucket_key=server_asset.s3_object_key
        )

        # Userdata executes script from S3
        application.user_data.add_execute_file_command(
            file_path=application_path,
            arguments="'"+ec.attr_read_end_point_addresses+"'"
        )
        application_asset.grant_read(application.role)

        server.user_data.add_execute_file_command(
            file_path=client_path,
            arguments="'"+ec.attr_read_end_point_addresses+"'"
        )
        rs_ag.user_data.add_execute_file_command(
            file_path=server_path,
            arguments="'"+ec.attr_read_end_point_addresses+"'"
        )
        client_asset.grant_read(server.role)


app = core.App()

EC2Stack(app, "SandBag")

app.synth()